import sys
import xlrd

if __name__ == '__main__':

    if not len(sys.argv) > 1:
        sys.exit("Usage: xl2wiki infile.xls [outfile]")

    # Read it in and build up a string in the mediawiki format
    book = xlrd.open_workbook(sys.argv[1])
    s = u""

    # Loop over all sheets
    for sheet in book.sheets():
        s += u"{}\n  \n  ".format(sheet.name)
        for row_index in xrange(sheet.nrows):
            for col_index in xrange(sheet.ncols):
                s += u"|{}".format(sheet.cell(row_index, col_index).value)
            s += u'|  \n'
        s += u'  \n'

    # Save it or print it
    if len(sys.argv) == 3:
        with open(sys.argv[2], 'w') as f:
            f.write(s.encode('utf-8'))
    else:
        print s
