#!/usr/bin/env python
"""Convert GFF3 gene predictions into protein sequences.

This program requires bcbb installed.

This works with the GFF3 output format:

##gff-version 3
##sequence-region Contig5.15 1 47390
Contig5.15      GlimmerHMM      mRNA    323     325     .       +       .       ID=Contig5.15.path1.gene1;Name=Contig5.15.path1.gene1
Contig5.15      GlimmerHMM      CDS     323     325     .       +       0       ID=Contig5.15.cds1.1;Parent=Contig5.15.path1.gene1;Name=Contig5.15.path1.gene1;Note=final-exon


Usage:
    gff_to_proteins.py <gff3> <ref fasta> <min len>   # produce protein file according min protein length
    gff_to_proteins.py <gff3> <ref fasta>             # produce protein file and its gene file from scaffold file
"""
from __future__ import with_statement
import sys
import os
import operator

from Bio import SeqIO
from Bio.SeqRecord import SeqRecord

from BCBio import GFF

def main(glimmer_file, ref_file, minProteinLen=None):
    with open(ref_file) as in_handle:
        ref_recs = SeqIO.to_dict(SeqIO.parse(in_handle, "fasta"))

    base, ext = os.path.splitext(glimmer_file)
    out_protein_file = "%s-proteins.fa" % base
    out_gene_file = "%s-gene.fa" % base
    out_split_protein_file = "%s-split-proteins.fa" % base
    if isinstance(minProteinLen, int):
        out_split_protein = open(out_split_protein_file, "w")
    else:
        out_protein = open(out_protein_file, "w")
        out_gene = open(out_gene_file, "w")
    for gene_seq, feature in protein_recs(glimmer_file, ref_recs):
        if isinstance(minProteinLen, int):
            proteinLis = gene_seq.translate().split('*')
            for ss in range(len(proteinLis)):
                if len(proteinLis[ss]) <= minProteinLen: continue
                splitProteinRec = SeqRecord(proteinLis[ss], feature.qualifiers["ID"][0]+'.%s' % ss, "", "")
                SeqIO.write(splitProteinRec, out_split_protein, "fasta")
        else:
            proteinRec = SeqRecord(gene_seq.translate(), feature.qualifiers["ID"][0], "", "")
            geneRec = SeqRecord(gene_seq, feature.qualifiers["ID"][0], "", "")
            SeqIO.write(proteinRec, out_protein, "fasta")
            SeqIO.write(geneRec, out_gene, "fasta")
    if isinstance(minProteinLen, int):
        out_split_protein.close()
    else:
        out_protein.close()
        out_gene.close()

def protein_recs(glimmer_file, ref_recs):
    """Generate protein records from GlimmerHMM gene predictions.
    """
    with open(glimmer_file) as in_handle:
        for rec in glimmer_predictions(in_handle, ref_recs):
            for feature in rec.features:
                seq_exons = []
                for cds in feature.sub_features:
                    seq_exons.append(rec.seq[
                        cds.location.nofuzzy_start:
                        cds.location.nofuzzy_end])
                gene_seq = reduce(operator.add, seq_exons)
                if feature.strand == -1:
                    gene_seq = gene_seq.reverse_complement()
                yield gene_seq, feature
                


def glimmer_predictions(in_handle, ref_recs):
    """Parse Glimmer output, generating SeqRecord and SeqFeatures for predictions
    """
    for rec in GFF.parse(in_handle, target_lines=1000, base_dict=ref_recs):
        yield rec

if __name__ == "__main__":
    if len(sys.argv) < 3 or len(sys.argv) > 4:
        print __doc__
        sys.exit()
    if len(sys.argv) == 4:
        minProteinLen = int(sys.argv[3])
        tmpArgs = sys.argv[1:3] + [minProteinLen]
        main(*tmpArgs)
    else:
        main(*sys.argv[1:])
