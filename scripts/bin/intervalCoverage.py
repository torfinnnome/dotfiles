#!/usr/bin/env python

import operator, sys

# https://code.google.com/p/bpbio/source/browse/trunk/interval_tree/interval_tree.py

class IntervalTree(object):
    __slots__ = ('intervals', 'left', 'right', 'center')

    def __init__(self, intervals, depth=24, minbucket=64, _extent=None, maxbucket=512):
        """\
        `intervals` a list of intervals *with start and stop* attributes.
        `depth`     the depth of the tree
        `minbucket` if any node in the tree has fewer than minbucket
                    elements, make it a leaf node
        `maxbucket` even it at specifined `depth`, if the number of intervals >
                    maxbucket, split the node, make the tree deeper.

        depth and minbucket usually do not need to be changed. if
        dealing with large numbers (> 1M) of intervals, the depth could
        be increased to 24.

        Useage:

         >>> ivals = [Interval(2, 3), Interval(1, 8), Interval(3, 6)]
         >>> tree = IntervalTree(ivals)
         >>> sorted(tree.find(1, 2))
         [Interval(2, 3), Interval(1, 8)]

        this provides an extreme and satisfying performance improvement
        over searching manually over all 3 elements in the list (like
        a sucker). 

        the IntervalTree class now also supports the iterator protocol
        so it's easy to loop over all elements in the tree:

         >>> import operator
         >>> sorted([iv for iv in tree], key=operator.attrgetter('start'))
         [Interval(1, 8), Interval(2, 3), Interval(3, 6)]


        NOTE: any object with start and stop attributes can be used
        in the incoming intervals list.
        """ 
	
        depth -= 1
        if (depth == 0 or len(intervals) < minbucket) and len(intervals) < maxbucket:
            self.intervals = intervals
            self.left = self.right = None
            return 

        if _extent is None:
            # sorting the first time through allows it to get
            # better performance in searching later.
            intervals.sort(key=operator.attrgetter('start'))

        left, right = _extent or \
               (intervals[0].start, max(i.stop for i in intervals))
        #center = intervals[len(intervals)/ 2].stop
        center = (left + right) / 2.0

        
        self.intervals = []
        lefts, rights  = [], []
        

        for interval in intervals:
            if interval.stop < center:
                lefts.append(interval)
            elif interval.start > center:
                rights.append(interval)
            else: # overlapping.
                self.intervals.append(interval)
                
        self.left   = lefts  and IntervalTree(lefts,  depth, minbucket, (intervals[0].start,  center)) or None
        self.right  = rights and IntervalTree(rights, depth, minbucket, (center,               right)) or None
        self.center = center
 
    def find(self, start, stop):
        """find all elements between (or overlapping) start and stop"""
        if self.intervals and not stop < self.intervals[0].start:
            overlapping = [i for i in self.intervals if i.stop >= start 
                                                    and i.start <= stop]
        else:
            overlapping = []

        if self.left and start <= self.center:
            overlapping += self.left.find(start, stop)

        if self.right and stop >= self.center:
            overlapping += self.right.find(start, stop)

        return overlapping

    def __iter__(self):
        if self.left:
            for l in self.left: yield l

        for i in self.intervals: yield i

        if self.right:
            for r in self.right: yield r
   
    # methods to allow un/pickling (by pzs):
    def __getstate__(self):
        return { 'intervals' : self.intervals,
                    'left'   : self.left,
                    'right'  : self.right,
                    'center' : self.center }

    def __setstate__(self, state):
        for key,value in state.iteritems():
            setattr(self, key, value)

class Interval(object):
    __slots__ = ('start', 'stop')
    def __init__(self, start, stop):
        self.start = start
        self.stop  = stop
    def __repr__(self):
        return "Interval(%i, %i)" % (self.start, self.stop)
    
    def __getstate__(self):
        return {'start': self.start, 
                'stop': self.stop }
    def __setstate__(self, state):
        for k, v in state.iteritems():
            setattr(self, k, v)

if __name__ == '__main__':
    """
        This script adds a column to a tsv file, representing the coverage of the intervals (chr, start, stop).
    """

    chromPos = int(sys.argv[2]) - 1 
    startPos = int(sys.argv[3]) - 1
    stopPos = int(sys.argv[4]) - 1
    # In LASTZ, opposite strand hits are marked by a separate strand-column
    if len(sys.argv) > 5:
        strandPos = int(sys.argv[5]) - 1
        sizePos = int(sys.argv[6]) - 1
    else:
        strandPos = -1
        sizePos = -1

    intervals = {}
   
    def brute_force_find(intervals, start, stop):
        return [i for i in intervals if i.stop >= start and i.start <= stop]
 
    with open(sys.argv[1]) as f:
      for line in f.readlines():
        if not line.startswith("#"):
          lineL = line.strip().split()
          chromHit = lineL[chromPos]
          startHit = lineL[startPos]
          stopHit = lineL[stopPos]

          startHit = int(startHit)
          stopHit = int(stopHit)
          
          if strandPos >= 0:
              strand = lineL[strandPos]
              if strand == '-':
                  size = int(lineL[sizePos])
                  tempstartHit = size - stopHit
                  stopHit = size - startHit
                  startHit = tempstartHit

          if startHit > stopHit:
              startHit, stopHit = stopHit, startHit
 
          if not chromHit in intervals:
              intervals[chromHit] = []
        
          intervals[chromHit].append(Interval(startHit, stopHit))

    # 2015-01-20, torfn: Speed things up:
    trees = {}
    for chrom in intervals:
      trees[chrom] = IntervalTree(intervals[chrom])


    # TODO: Move duplicated code into a function.
    with open(sys.argv[1]) as f:
      for line in f.readlines():
        if not line.startswith("#"):
          lineL = line.strip().split()
          chromHit = lineL[chromPos]
          startHit = lineL[startPos]
          stopHit = lineL[stopPos]
        
          startHit = int(startHit)
          stopHit = int(stopHit)

          if strandPos >= 0:
              strand = lineL[strandPos]
              if strand == '-':
                  size = int(lineL[sizePos])
                  tempstartHit = size - stopHit
                  stopHit = size - startHit
                  startHit = tempstartHit

          if startHit > stopHit:
              startHit, stopHit = stopHit, startHit

          if chromHit in intervals:
#              print line.strip() + "\t" + str(len(brute_force_find(intervals[chromHit], startHit, stopHit)))
              # 2015-01-20, torfn: Speed things up:
              print line.strip() + "\t" + str(len(trees[chromHit].find(startHit, stopHit)))
        else:
          print line.strip() + "\tposCoverage"

