#!/bin/bash


TMPDIR=$SCRATCH/torfn/conv
mkdir -p $TMPDIR

INPUT="$*"
OUTPUTDIR=$(dirname "$INPUT")
INFILE=$(basename "$INPUT")
EXTENSION=${INFILE##*.}
IN=$(basename "$INFILE" $EXTENSION)
OUTPUT="${OUTPUTDIR}/${IN}m4v"

INTEMP="$TMPDIR/${INFILE}"
OUTTEMP="$TMPDIR/${IN}m4v"

RCLONEROOT="Plex_gdrive:"

[[ $EXTENSION == "mp4" ]] && exit
[[ $EXTENSION == "m4v" ]] && exit
[[ $EXTENSION == "h264" ]] && exit

module load singularity
HANDBRAKECLI="singularity exec /mnt/users/torfn/Singularity/handbrake-cli-latest.simg HandBrakeCLI"

rclone copy -v ${RCLONEROOT}"$INPUT" "$TMPDIR"

if [ -f "$INTEMP" ];
then
        time $HANDBRAKECLI -e x264  -q 20.0 -a 1 -E ffaac -B 160 -6 dpl2 -R Auto -D 0.0 --audio-copy-mask aac,ac3,dtshd,dts,mp3 --audio-fallback ffac3 -f mp4 --loose-anamorphic --modulus 2 -m --x264-preset veryfast --h264-profile main --h264-level 4.0 -i "$INTEMP" -o "$OUTTEMP"
		#time h265ize -v -m medium -q 20 -x --no-sao --aq-mode 3 --stats

        if [ -f "$OUTTEMP" ];
        then
                rclone copyto -v "$OUTTEMP" ${RCLONEROOT}"$OUTPUT"
                rclone moveto -v ${RCLONEROOT}"${INPUT}" ${RCLONEROOT}"${INPUT}.converted"
                rm "$INTEMP"
                rm "$OUTTEMP"
        fi
fi

