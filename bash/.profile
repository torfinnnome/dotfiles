
export PS1="\[\e[32;1m\](\[\e[37;0m\]\u@\h\[\e[32;1m\])-(\[\e[37;0m\]\w\[\e[32;1m\])\n$ \[\e[0m\]"


GITCOMPL=~/bin/git-completion.bash
if [ -f $GITCOMPL ]; then
  . $GITCOMPL
fi

alias todo='todo.sh -d "$HOME/Dropbox/Applications/Preferences-Sync/todo/todo.cfg"'
alias joe='joe --wordwrap'
alias grep='grep --color=auto'
alias vim=nvim

#. ~/bin/source-todo_completion
#. ~/bin/source-sshuttle_helpers

export LC_ALL=en_US.UTF-8  
export LANG=en_US.UTF-8

if [ -f ~/bin/autojump/etc/profile.d/autojump.sh ]; then
	source ~/bin/autojump/etc/profile.d/autojump.sh; fi

# Log all commands:
source ~/bin/hcmnt
export hcmntextra='date "+%Y%m%d %R"'
#export PROMPT_COMMAND='hcmnt -l ~/.bash_history-logged'
export PROMPT_COMMAND='hcmnt -eityl ~/.hcmnt.log $LOGNAME@$HOSTNAME'
hgrepfunc() {
 grep -i $1 ~/.hcmnt.log
}
alias hgrep=hgrepfunc

export R_LIBS_USER="$R_LIBS_USER:~srsand/R/x86_64-redhat-linux-gnu-library/3.0"

# GitHub API token
export HOMEBREW_GITHUB_API_TOKEN=472845907f43ac8e28a923583851cf661d6b3805

gzip() {
 pigz "$@"
 }
export -f gzip
 
gunzip() {
  unpigz "$@"
  }
export -f gunzip
  
test -e ${HOME}/.iterm2_shell_integration.bash && source ${HOME}/.iterm2_shell_integration.bash

alias rmate-ssh="bash rmate-ssh"
export GOPATH=~/go

#export RSTUDIO_WHICH_R=/usr/local/bin/R

export PATH="~/bin:$PATH:/usr/local/sbin"
source /etc/profile.d/cigene.sh

