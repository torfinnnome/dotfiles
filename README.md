The dotfiles of Torfinn Nome.

Use with [GNU Stow](https://www.gnu.org/software/stow/).

(Inspired by [cfdrake/dotfiles](https://github.com/cfdrake/dotfiles).)
